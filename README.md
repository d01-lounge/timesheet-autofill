# timesheet-autofill #

This is the awesome timesheet-autofill script!

### How can I check this awesome script out? ###

* npm install
* modify `script.js`
* gulp uglify

### How do I make a bookmark of this awesome script? ###

* open `script.min.js` & copy all of its RAW contents
* inspect element to the first <a>-tag you'll find on this page.
* clear `href`-attribute and fill in: `javascript:`
* right after `javascript:`, paste your copied code.
* Now drag this link to your bookmarks bar
* PROFIT
* if all above fails, go to [timesheets.mhx.be](http://mhx.be/timesheets/) and drag the blue button to the bookmarks-bar.
### How do I use this awesome script? ###

* click on the bookmark. (You will be redirected if you're not on timesheets.cronos.be)
* fill in a TimeSheet-code. (copy + paste works best. Watch out for lost spaces.. hate those guys!)
* fill in the amount of hours you work-a-day.
* be amazed of magic that you've just witnessed
* after short review, click submit to make sure to save these awesome changes.


### How do I contribute? ###

* Slack Mike
* OR just do it yourself!