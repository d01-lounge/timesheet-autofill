(function (document) {

    if (window.location.hostname !== 'timesheets.cronos.be') {
        window.location = 'https://timesheets.cronos.be';
        return;
    }

    // Variables ----------
    var d = document,
        workTable = Array.prototype.slice.call(d.querySelectorAll('#workTable tr')),
        feestdagen = [],
        availableTSCodes = [],
        TSCode = window.prompt('Plak hier een bestaande TSCode in.'),
        hoursInADay = window.prompt('Hoeveel uur werkt u op een dag?');

    // remove subtotals row
    workTable.shift();
    workTable.pop();

    // Check if user has given us proper data.
    if (typeof TSCode !== 'string') {
        alert('Gelieve een TScode in te geven.');
        return;
    }

    if (typeof parseInt(hoursInADay) !== 'number') {
        console.error('Gelieve een nummer in te geven.');
        return;
    }

     // Functions ----------
    function loopTSCodeRow(row) {

        row.days.forEach(function (day, index) {
            switch (day.className) {
                case 'del':
                case 'Pr':
                case 'lt':
                case 'rTot':
                    // Ignore following fields
                    // 1. delete button
                    // 2. TimesheetCode Title
                    // 3. Weekends (saturday & sunday)
                    // 4. Total field
                    break;

                default:
                    if (!day.className.length || !day.children[0].value.length) {
                        // Ignore Holidays
                        if (feestdagen.indexOf(index) === -1) {
                            day.children[0].value = hoursInADay;
                        }
                    }

                    break;
            }
        });
    }

    // Actual Logic ----------

    // remove feestdagen row + convert children to Array
    workTable.forEach(function(w, index) {
        // Convert children(<td>)-NodeList to Array;
        w.days = Array.prototype.slice.call(w.children);

        availableTSCodes.push(w.days[1].innerText);

        // If TSCode contains 'Feestdagen', make sure to remove it.
        if (w.innerText.indexOf('Feestdagen') !== -1) {
            var feestRow = workTable.splice(index, 1);
            feestRow[0].days.forEach(function (f, index) {
                // Track indexes of holidays, so we can ignore these days in loopTSCodeRow();
                if (f.className === 'vt') {
                    feestdagen.push(index);
                }
            });
        }
    });

    // Check if Requested TSCode is actually available.
    if (availableTSCodes.indexOf(TSCode) === -1) {
        alert('`' + TSCode + '` is niet beschikbaar als TSCode!\nGelieve een geldige TSCode in te geven.');
        return;
    }

    workTable.forEach(function (w) {
        if (w.innerText.indexOf(TSCode) !== -1) {
            loopTSCodeRow(w);
        }
    });

    // Manipulate DOM to make some 'reklam'.'
    var menuTr = document.querySelector('.dxmMenu.dxmMenuQF tr');

    if (menuTr.children.length === 3) {
        var link = document.createElement('a');
        link.setAttribute('href', 'https://bitbucket.org/d01-lounge/timesheet-autofill');
        link.setAttribute('style', 'background-color:black;color:white;padding:3px;');
        link.setAttribute('target', '_blank');
        link.innerText = 'A DO1-Lounge project by Mike Henderyckx';

        $(menuTr).append(link);
    }

})(document);